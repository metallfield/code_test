Choose ONE of the following tasks.
Please do not invest more than two hours on this.
Upload your results to a Github Gist, for easier sharing and reviewing.

Thank you and good luck!



Code to refactor
=================
1) app/Http/Controllers/BookingController.php
2) app/Repository/BookingRepository.php

Code to write tests
=====================
3) App/Helpers/TeHelper.php method willExpireAt
4) App/Repository/UserRepository.php, method createOrUpdate




What I expect:

A. A readme with:

1.  Your thoughts about the code. What makes it amazing code. Or what makes it ok code. Or what makes it terrible code. How would you have done it. Thoughts on formatting. Logic.. 
2.  Refactor it if you feel it needs formatting. The more love you put into it. The easier for us to asses.  

Make two commits. First commit with original code. Second with your refactor so we can easily trace changes. 

Vänliga hälsningar / Best regards
Virpal Singh





My thoughts
===============
Just as you asked: my impressions are overall positive, the code's logic is broken down pretty well.
In the controller and the repository I've noticed unused variables. I've removed them and also moved a part of checks from the controller's method - distanceFeed to the repository BookingRepository into the distanceFeedUpdate method to relieve the controller's logic. I haven't changed BookingRepository, since it's a rather big repository and it's hard to make any changes without knowing the whole app's logic.
Positive sides of the code - it's relieves the controller's logic with repositories, but as a downside - repository's methods (functions) are rather big and hard to read. At a first look it seems that parts of certain methods should be broken down into smaller methods, and also the unused variables.
